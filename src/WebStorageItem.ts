export class WebStorageItem {

    constructor(public key: string, public value: string){}
}