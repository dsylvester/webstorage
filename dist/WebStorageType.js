"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WebStorageType;
(function (WebStorageType) {
    WebStorageType[WebStorageType["Session"] = 1] = "Session";
    WebStorageType[WebStorageType["Local"] = 2] = "Local";
})(WebStorageType = exports.WebStorageType || (exports.WebStorageType = {}));
//# sourceMappingURL=WebStorageType.js.map