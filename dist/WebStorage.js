"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("./index");
class WebStorage {
    set tempStorage(value) {
        this._tempStorage = value;
    }
    get tempStorage() {
        return this._tempStorage;
    }
    constructor(webStorageType) {
        this.GetStorageInstance(webStorageType);
    }
    Add(key, value) {
        this.tempStorage.setItem(key, value);
    }
    Get(key) {
        return this.tempStorage.getItem(key);
    }
    Remove(key) {
        this.tempStorage.removeItem(key);
    }
    Clear() {
        this.tempStorage.clear();
    }
    ShowAll() {
        let keys = this.GetAllKeys();
        if (keys.length === 0) {
            return null;
        }
        let result = new Array();
        keys.forEach((x) => {
            result.push(new index_1.WebStorageItem(x, this.Get(x)));
        });
        return result;
    }
    GetAllKeys() {
        if (this.tempStorage.length === 0) {
            return null;
        }
        let result = new Array();
        for (let i = 0; i < this.tempStorage.length; i++) {
            console.log(this.tempStorage.key(i));
            result.push(this.tempStorage.key(i));
        }
        return result;
    }
    GetStorageInstance(webStorageType) {
        this.GetInstance(webStorageType);
    }
    hasWebStorage() {
        if (window.sessionStorage === undefined ||
            window.localStorage === undefined) {
            return false;
        }
        return true;
    }
    GetInstance(webStorageType) {
        if (this.hasWebStorage() === false) {
            return null;
        }
        switch (webStorageType) {
            case index_1.WebStorageType.Session:
                this.tempStorage = window.sessionStorage;
                break;
            case index_1.WebStorageType.Local:
                this.tempStorage = window.localStorage;
                break;
        }
    }
}
exports.WebStorage = WebStorage;
//# sourceMappingURL=WebStorage.js.map