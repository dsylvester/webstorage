"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class WebStorageItem {
    constructor(key, value) {
        this.key = key;
        this.value = value;
    }
}
exports.WebStorageItem = WebStorageItem;
//# sourceMappingURL=WebStorageItem.js.map