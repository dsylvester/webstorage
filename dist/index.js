"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./WebStorage"));
__export(require("./WebStorageType"));
__export(require("./WebStorageItem"));
//# sourceMappingURL=index.js.map